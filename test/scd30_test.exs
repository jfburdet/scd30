defmodule SCD30Test do
  use ExUnit.Case
  doctest SCD30

  alias SCD30.Utils.CRC

  @doc """
  binary sequence taken form https://cdn-learn.adafruit.com/assets/assets/000/098/501/original/Sensirion_CO2_Sensors_SCD30_Interface_Description.pdf?1609963135
  page 4
  """
  test "test valid crc processing" do
    assert SCD30.Utils.CRC.crc_check_filter(
             <<0x43, 0xDB, 0xCB, 0x8C, 0x2E, 0x8F, 0x41, 0xD9, 0x70, 0xE7, 0xFF, 0xF5, 0x42, 0x43,
               0xBF, 0x3A, 0x1B, 0x74>>
           ) == <<0x43, 0xDB, 0x8C, 0x2E, 0x41, 0xD9, 0xE7, 0xFF, 0x42, 0x43, 0x3A, 0x1B>>
  end

  @doc """
  Same as 'test valid crc processing' test, but with first byte changed
  that make crc failing.
  """
  test "test invalid crc processing" do
    assert SCD30.Utils.CRC.crc_check_filter(
             <<0x42, 0xDB, 0xCB, 0x8C, 0x2E, 0x8F, 0x41, 0xD9, 0x70, 0xE7, 0xFF, 0xF5, 0x42, 0x43,
               0xBF, 0x3A, 0x1B, 0x74>>
           ) == :crc_error
  end

  @doc """
  Taken from same doc as above, but page 5.
  """
  test "test crc" do
    assert CRC.crc(<<0xBE, 0xEF>>) == 0x92
  end
end
