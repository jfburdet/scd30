defmodule SCD30.MixProject do
  use Mix.Project

  @source_url "https://gitlab.com/jfburdet/scd30"
  @version "0.1.2"

  def project do
    [
      app: :scd30,
      version: @version,
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      docs: docs(),
      package: package(),
      source_url: @source_url
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp description() do
    "Driver for the Adafruit SCD-30 CO2 temperature and humidity sensor"
  end

  defp docs do
    [
      extras: ["README.md"],
      main: "readme",
      source_ref: "v#{@version}",
      source_url: @source_url
    ]
  end

  defp deps do
    [
      {:circuits_i2c, "~> 1.0"},
      {:crc, "~> 0.10"},
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false}
    ]
  end

  defp package() do
    [
      name: "scd30",
      files: ["lib", "mix.exs", "README.md", "LICENSE"],
      licenses: ["Apache-2.0"],
      links: %{
        "GitLab" => @source_url,
        "SCD30 Datasheet" =>
          "https://cdn-learn.adafruit.com/assets/assets/000/098/501/original/Sensirion_CO2_Sensors_SCD30_Interface_Description.pdf?1609963135"
      }
    ]
  end
end
