defmodule SCD30.Measurement do
  @type t() :: %__MODULE__{
          co2_ppm: Float.t(),
          rh_percent: Float.t(),
          temp_celsius: Float.t()
        }

  defstruct co2_ppm: :unknown,
            rh_percent: :unknown,
            temp_celsius: :unknown
end
