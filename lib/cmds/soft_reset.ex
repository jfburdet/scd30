defmodule SCD30.Cmds.SOFT_RESET do
  @moduledoc """
  Soft reset.
  """
  alias SCD30.Utils.I2C

  @cmd [0xD3, 0x04]

  @spec restart_sensor(reference, byte) :: :ok
  def restart_sensor(i2c, device_addr) do
    I2C.write(i2c, device_addr, [
      @cmd
    ])
  end
end
