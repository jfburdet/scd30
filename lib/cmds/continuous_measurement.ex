defmodule SCD30.Cmds.CONTINUOUS_MEASUREMENT do
  @moduledoc """
  Continuous measurement mode.
  """

  alias SCD30.Utils.I2C
  alias SCD30.Utils.CRC

  @start_cmd [0x00, 0x10]
  @stop_cmd [0x01, 0x04]

  @spec start(any, any, any) :: :ok | {:error, :pressure_out_of_range}
  def start(i2c, device_addr, pressure \\ 0)

  def start(i2c, device_addr, pressure) when pressure >= 0 do
    I2C.write(i2c, device_addr, [
      @start_cmd,
      <<pressure::16>>,
      CRC.crc(<<pressure::16>>)
    ])
  end

  def start(_, _, _) do
    {:error, :pressure_out_of_range}
  end

  @spec stop(reference, byte) :: :ok
  def stop(i2c, device_addr) do
    I2C.write(i2c, device_addr, [@stop_cmd])
  end
end
