defmodule SCD30.Cmds.MEASUREMENT_INTERVAL do
  @moduledoc """
  Set measurement interval.
  """

  alias SCD30.Utils.I2C
  alias SCD30.Utils.CRC
  require Logger

  @set_cmd [0x46, 0x00]
  @spec set(reference, byte, non_neg_integer) :: :ok | {:error, :interval_out_of_range}
  def set(i2c, device_addr, interval \\ 2)

  def set(i2c, device_addr, interval) when interval in 2..1800 do
    I2C.write(i2c, device_addr, [@set_cmd, <<interval::16>>, CRC.crc(<<interval::16>>)])
  end

  def set(_, _, _) do
    {:error, :interval_out_of_range}
  end
end
