defmodule SCD30.Cmds.TEMP_OFFSET do
  @moduledoc """
  Temperature Offset.
  """

  alias SCD30.Utils.I2C
  alias SCD30.Utils.CRC

  @cmd [0x54, 0x03]

  @spec set(reference, byte, number) :: :ok
  def set(i2c, device_addr, temp_offset \\ 0)

  def set(i2c, device_addr, temp_offset) when temp_offset >= 0 do
    val_to_write = <<trunc(temp_offset * 100)::16>>

    I2C.write(i2c, device_addr, [
      @cmd,
      val_to_write,
      CRC.crc(val_to_write)
    ])
  end

  def set(_, _, _, _) do
    {:error, :temp_offset_out_of_range}
  end
end
