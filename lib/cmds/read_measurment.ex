defmodule SCD30.Cmds.READ_MEASUREMENT do
  @moduledoc """
  Read measurement.
  """
  alias SCD30.Utils.I2C
  alias SCD30.Utils.CRC

  @read_cmd [0x03, 0x00]

  @spec read(reference, byte) :: :error | SCD30.Measurement.t()
  def read(i2c, device_addr) do
    :ok = I2C.write(i2c, device_addr, @read_cmd)

    case I2C.read(i2c, device_addr, 18) do
      {:ok, data} -> data |> CRC.crc_check_filter() |> decode()
      _ -> :error
    end
  end

  defp decode(<<co2::float-size(32), temp::float-size(32), rh::float-size(32)>>) do
    %SCD30.Measurement{
      co2_ppm: co2,
      rh_percent: rh,
      temp_celsius: temp
    }
  end
end
