defmodule SCD30.Cmds.FRC do
  @moduledoc """
  Forced Recalibration value (FRC).
  """

  alias SCD30.Utils.I2C
  alias SCD30.Utils.CRC

  @cmd [0x52, 0x04]

  @spec set(reference, byte, non_neg_integer) :: :ok
  def set(i2c, device_addr, co2_ppm) when co2_ppm >= 0 do
    I2C.write(i2c, device_addr, [
      @cmd,
      <<co2_ppm::16>>,
      CRC.crc(<<co2_ppm::16>>)
    ])
  end
end
