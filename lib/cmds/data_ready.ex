defmodule SCD30.Cmds.DATA_READY do
  @moduledoc """
  Data ready.
  """

  alias SCD30.Utils.I2C
  alias SCD30.Utils.CRC

  @read_cmd [0x02, 0x02]

  @spec read(reference, byte) :: :error | false | true
  def read(i2c, device_addr) do
    case I2C.write_read(i2c, device_addr, @read_cmd, 3) do
      {:ok, data} -> data |> CRC.crc_check_filter() |> decode()
      _ -> :error
    end
  end

  defp decode(<<ready_msb::8, ready_lsb::8>>) do
    <<ready::16>> = <<ready_msb, ready_lsb>>

    ready == 1
  end
end
