defmodule SCD30.Cmds.ALITUTUDE_COMPENSATION do
  @moduledoc """
  Altitude Compensation.
  """

  alias SCD30.Utils.I2C
  alias SCD30.Utils.CRC

  @cmd [0x51, 0x02]

  @spec set(reference, byte, non_neg_integer) :: :ok | {:error, :altitude_out_of_range}
  def set(i2c, device_addr, altitude \\ 0)

  def set(i2c, device_addr, altitude) when altitude >= 0 do
    I2C.write(i2c, device_addr, [
      @cmd,
      <<altitude::16>>,
      CRC.crc(<<altitude::16>>)
    ])
  end

  def set(_, _, _) do
    {:error, :altitude_out_of_range}
  end
end
