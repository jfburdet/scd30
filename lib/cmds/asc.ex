defmodule SCD30.Cmds.ASC do
  @moduledoc """
  Automatic Self-Calibration (ASC).
  """

  alias SCD30.Utils.I2C
  alias SCD30.Utils.CRC

  @cmd [0x53, 0x56]

  @spec activate(reference, byte) :: :ok
  def activate(i2c, device_addr) do
    I2C.write(i2c, device_addr, [
      @cmd,
      <<1::16>>,
      CRC.crc(<<1::16>>)
    ])
  end

  @spec deactivate(reference, byte) :: :ok
  def deactivate(i2c, device_addr) do
    I2C.write(i2c, device_addr, [
      @cmd,
      <<0::16>>,
      CRC.crc(<<0::16>>)
    ])
  end
end
