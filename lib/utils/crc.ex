defmodule SCD30.Utils.CRC do
  @crc_spec %{
    width: 8,
    poly: 0x31,
    init: 0xFF,
    refin: false,
    refout: false,
    xorout: 0x00
  }

  def crc(<<_byte1::8, _byte2::8>> = word) do
    CRC.calculate(word, @crc_spec)
  end

  def crc_check_filter(data) do
    data |> crc_check_filter_rec(<<>>)
  end

  defp crc_check_filter_rec(<<>>, acc) do
    acc
  end

  defp crc_check_filter_rec(<<word::16, crc::8, rest::bitstring>>, acc) do
    case crc(<<word::16>>) do
      ^crc -> crc_check_filter_rec(rest, acc <> <<word::16>>)
      _ -> :crc_error
    end
  end
end
