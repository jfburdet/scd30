defmodule SCD30.Utils.I2C do
  @moduledoc """
  I2C wrappers introducing delays.

  SCD30 module seems unable to handle quick write_read, so I had to write those wrappers.
  The sleep delay is taken from the Adafruit python library :
  https://github.com/adafruit/Adafruit_CircuitPython_SCD30/blob/11412384a1b3c9b8121c282e49786dd0f9a13f91/adafruit_scd30.py#L281

  In the above file, call to python sleep is 50ms, but written as 3ms in comment ? I choosed empirically to use 10ms.
  """

  def write_read(i2c_bus, address, write_data, bytes_to_read, opts \\ []) do
    :ok = Circuits.I2C.write(i2c_bus, address, write_data, opts)
    Circuits.I2C.read(i2c_bus, address, bytes_to_read, opts)
  end

  def write(i2c_bus, address, write_data, opts \\ []) do
    :ok = Circuits.I2C.write(i2c_bus, address, write_data, opts)
    :timer.sleep(10)
    :ok
  end

  def read(i2c_bus, address, bytes_to_read, opts \\ []) do
    Circuits.I2C.read(i2c_bus, address, bytes_to_read, opts)
  end
end
