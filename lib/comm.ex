defmodule SCD30.Comm do
  alias Circuits.I2C

  def discover(possible_addresses \\ [0x61]) do
    I2C.discover_one!(possible_addresses)
  end

  def open(bus_name) do
    {:ok, i2c} = I2C.open(bus_name)
    i2c
  end
end
