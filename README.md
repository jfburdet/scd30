# SCD30

[![Hex version](https://img.shields.io/hexpm/v/scd30.svg "Hex version")](https://hex.pm/packages/scd30)

Driver for the [Adafruit SCD-30 CO2 temperature and humidity sensor](https://learn.adafruit.com/adafruit-scd30).

* [Adafruit datasheet](https://cdn-learn.adafruit.com/assets/assets/000/098/501/original/Sensirion_CO2_Sensors_SCD30_Interface_Description.pdf?1609963135)
* [Chip manufacturer documentation](https://sensirion.com/products/downloads/?filter_category=53cf3e00-c25a-41f5-a310-8e6837dcaa5f)


## Installation

```elixir
def deps do
  [
    {:scd30, "~> 0.1"}
  ]
end
```

## Sample usage

### Using sensor commands

```elixir
iex(1)> {bus_name, device_addr} = SCD30.Comm.discover
{"i2c-1", 97}
iex(2)> i2c = SCD30.Comm.open(bus_name)
#Reference<0.2263858602.268566537.255722>
iex(3)> SCD30.Cmds.MEASUREMENT_INTERVAL.set(i2c, device_addr, 2) # Sensor will take measurement every 2 seconds
:ok
iex(4)> SCD30.Cmds.CONTINUOUS_MEASUREMENT.start(i2c, device_addr) # Starting continuous measurement mode
:ok
iex(6)> # Since sensor juste start continuous measurement we must wait a long time.
iex(7)> # after this startup, measurements will be available every interval.
iex(8)> :timer.sleep(20_000)  
:ok
iex(9)> if SCD30.Cmds.DATA_READY.read(i2c, device_addr),do: SCD30.Cmds.READ_MEASUREMENT.read(i2c, device_addr)
%SCD30.Measurement{
  co2_ppm: 1035.865478515625,
  rh_percent: 44.5159912109375,
  temp_celsius: 20.65003204345703
}
```

### Using GenServer

```elixir
iex(1)> SCD30.start_link([name: :scd30])
{:ok, #PID<0.1393.0>}
iex(2)> :timer.sleep(20_000)
:ok
iex(3)> GenServer.call(:scd30, :measure)
{:ok,
 %SCD30.Measurement{
   co2_ppm: 982.560791015625,
   rh_percent: 44.9462890625,
   temp_celsius: 20.49248504638672
 }}
```
